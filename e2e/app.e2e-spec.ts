import { Ngauth0Page } from './app.po';

describe('ngauth0 App', () => {
  let page: Ngauth0Page;

  beforeEach(() => {
    page = new Ngauth0Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
